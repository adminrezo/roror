# frozen_string_literal: true

Devise.setup do |config|
  config.mailer_sender = ENV['MAIL_SENDER']
  require 'devise/orm/active_record'
  config.case_insensitive_keys = [:email]
  config.strip_whitespace_keys = [:email]
  config.skip_session_storage = [:http_auth]
  config.stretches = Rails.env.test? ? 1 : 12
  config.reconfirmable = true
  config.expire_all_remember_me_on_sign_out = true
  config.password_length = 10..128
  config.email_regexp = /\A[^@\s]+@[^@\s]+\z/
  config.timeout_in = 15.minutes
  config.maximum_attempts = 5
  # config.unlock_in = 1.hour
  config.reset_password_within = 6.hours
  # config.encryptor = :sha512
  config.sign_out_via = :delete
end
