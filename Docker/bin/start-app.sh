#!/bin/sh

cd /home/k2 || exit 255
source .env
./bin/rails db:prepare || exit 255
./bin/rails webpacker:compile
./bin/rails server
