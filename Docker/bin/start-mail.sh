#!/bin/sh

echo "relay_domains = $MAIL_DOMAIN" >> /etc/postfix/main.cf
postconf -e "maillog_file=/dev/stdout"
/usr/libexec/postfix/master -d
