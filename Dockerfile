 ################
#  K2 app build  #
 ################

FROM ruby AS k2-app

ENV RAILS_ENV production

# Packages (as root)

RUN apt update && apt -y install \
    curl \
    libpq-dev \
    nmap \
    nodejs \
    wkhtmltopdf
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt update && apt -y install yarn

# App (as user)

RUN groupadd --gid 1000 k2
RUN adduser k2 --system --shell /bin/bash --uid 1000 --gid 1000 --disabled-password
USER k2
WORKDIR /home/k2
COPY --chown=k2:k2 . /home/k2
RUN yarn install
RUN bundle install
RUN curl -L https://github.com/OWASP/Amass/releases/download/v3.15.0/amass_linux_amd64.zip > amass.zip
RUN unzip amass.zip && rm amass.zip

EXPOSE 3000
CMD ["/home/k2/Docker/bin/start-app.sh"]
