# Roror

Roror for "Ruby on Rails on Rails" is my start pack for my projects in Rails.

It includes WebUI (Bulma, Font Awesome, Templates) and
 technical (Devise, Cancancan, i18n, ...) components.

## Start

A new project is started with :

  git clone https://gitlab.com/adminrezo/roror.git
  cd roror && ./bin/roror-init.sh


## So what to do next ?

Next you can :

- Create models, controllers, scaffolds, ... what you want.
- Manage rights with roles and Cancancan
- Modify README
- Modify Docker
