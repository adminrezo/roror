#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'


usage() {
  echo "Usage: $(basename $0) ApplicationName"
  echo "  - name : [a-zA-Z0-9-]"
  exit 1
}

[ ! $1 ] && usage
APPNAME="$1"
[[ ! $APPNAME =~ ^[a-zA-Z0-9-]+$ ]] && usage
APPNAME2=$(echo "$APPNAME" | tr -dc '[:alnum:]\n\r')

echo "Your project will be named $APPNAME"
echo -e "$GREEN[Enter]$NC | $RED<CTRL+C>$NC"
echo
read

echo -e "Files changed ...\n"
ZELIST=$(grep -r "THENAME" * --exclude-dir={tmp,bin,public,node_modules} 2>/dev/null |cut -d':' -f1 |sort -u)
for FILE in $ZELIST
do
  if [ $FILE == "config/application.rb" ]
  then
    sed -i "s/THENAME/$APPNAME2/" $FILE
  else
    sed -i "s/THENAME/$APPNAME/" $FILE
  fi
  echo "  - $FILE : done"
done

[ -x "bin/pre-commit" ] && cp -a bin/pre-commit .git/hooks/pre-commit

echo -e "\n${GREEN}Done !${NC}\nYour project is renamed ${GREEN}${APPNAME}${NC}"
